package drogueria;

import java.util.Scanner;

/**
 *
 * @author a16daviddss
 */
public class Drogueria {

    public static void main(String[] args) {
        String nombre;
        int p_inicial, p_final;
        Scanner scan = new Scanner(System.in);
        System.out.printf("ARTÍCULO: ");
        nombre=scan.next();
        System.out.printf("PRECIO INICIAL: ");
        p_inicial=Integer.parseInt(scan.next());
        System.out.printf("PRECIO FINAL: ");
        p_final=Integer.parseInt(scan.next());
        float tasa = Calculo(nombre, p_inicial, p_final);
        System.out.printf("\nARTÍCULO: %s\nVARIACIÓN DE PRECIO: %d\tEUROS\n%%VARIACIÓN: %.2f%%\n"
                ,nombre,(p_final-p_inicial),tasa);
    }

    public static float Calculo(String nombre, int p_inicial, int p_final) {
        float tasa;
        tasa = ((p_final - p_inicial) * 100) / p_inicial;
        return tasa;
    }
}

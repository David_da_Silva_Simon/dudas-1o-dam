package ejerciciopaganavidad;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author david
 */
public class Visualizar {

    public static void nomina() throws IOException {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        System.out.printf("Introduce el nombre del empleado: ");
        String nombre = read.readLine();
        System.out.printf("Introduce el sueldo base del empleado: ");
        int sBase = Integer.parseInt(read.readLine());
        System.out.printf("Introduce la categoría del empleado (A,B,C): ");
        char cat = Character.toUpperCase(read.readLine().charAt(0));
        System.out.printf("Introduce el numero de hijos del empleado: ");
        byte hijos = Byte.parseByte(read.readLine());
        Empleado e = new Empleado(nombre, sBase, cat, hijos);
        e.calculo();
    }

    public static void visualiza(String err, Empleado e, float sueldoF) {
        if (err != null) {
            System.err.println(err);
        } else {
            System.out.printf("EXTRA DE NAVIDAD DE:..... %s\n"
                    + "POR SUELDO(%d X %.2f) = %.2f\n"
                    + "POR HIJOS (100 X %d)\t= %d\n"
                    + "\tTOTAL\t\t= %.2f€\n", Character.toUpperCase(e.getNombre().charAt(0))
                    + e.getNombre().substring(1), e.getSueldoB(), e.getPorc(),
                    e.getSueldoB() * e.getPorc(), e.getHijos(), e.getHijos() * e.getBonusHijo(), sueldoF);
        }
    }
}

package ejerciciopaganavidad;

/**
 *
 * @author david
 */
public class Empleado {

    private String nombre;
    private int sueldoB;
    private char categoría;
    private byte hijos;
    private final int bonusHijo = 100;
    private float porc;

    public Empleado(String nombre, int sueldoB, char categoría, byte hijos) {
        this.nombre = nombre;
        this.sueldoB = sueldoB;
        this.categoría = categoría;
        this.hijos = hijos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSueldoB() {
        return sueldoB;
    }

    public void setSueldoB(int sueldoB) {
        this.sueldoB = sueldoB;
    }

    public char getCategoría() {
        return categoría;
    }

    public void setCategoría(char categoría) {
        this.categoría = categoría;
    }

    public byte getHijos() {
        return hijos;
    }

    public void setHijos(byte hijos) {
        this.hijos = hijos;
    }

    public float getPorc() {
        return porc;
    }

    public void setPorc(float porc) {
        this.porc = porc;
    }

    public int getBonusHijo() {
        return bonusHijo;
    }
    
    public void calculo() {
        float sueldoF = 0;
        String error = null;
        switch (this.categoría) {
            case 'A':
                sueldoF = this.sueldoB * 1.05f + this.bonusHijo * this.hijos;
                this.porc = 1.05f;
                break;
            case 'B':
                sueldoF = this.sueldoB * 1.10f + this.bonusHijo * this.hijos;
                this.porc = 1.10f;
                break;
            case 'C':
                sueldoF = this.sueldoB * 1.15f + this.bonusHijo * this.hijos;
                this.porc = 1.15f;
                break;
            default:
                error = "Categoría errónea";
                break;
        }
        Visualizar.visualiza(error, this, sueldoF);
    }
}

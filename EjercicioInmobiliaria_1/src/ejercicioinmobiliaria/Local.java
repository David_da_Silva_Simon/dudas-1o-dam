package ejercicioinmobiliaria;

/**
 *
 * @author a16daviddss
 */
public class Local {

    private String tipoLocal;
    private float precioI;
    private int mensualidades;
    private String comprador;
    private float intereses;

    public Local(String tipoLocal, float precioI) {
        this.tipoLocal = tipoLocal;
        this.precioI = precioI;
    }

    public String getTipoLocal() {
        return tipoLocal;
    }

    public void setTipoLocal(String tipoLocal) {
        this.tipoLocal = tipoLocal;
    }

    public float getPrecioI() {
        return precioI;
    }

    public void setPrecioI(float precioI) {
        this.precioI = precioI;
    }

    public int getMensualidades() {
        return mensualidades;
    }

    public void setMensualidades(int mensualidades) {
        this.mensualidades = mensualidades;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public float getIntereses() {
        return intereses;
    }

    public void setIntereses(float intereses) {
        this.intereses = intereses;
    }

    public void calculo() {
        float precioF = 0;
        String error = null;
        boolean salir = false;

        switch (this.tipoLocal) {
            case "1":
                this.tipoLocal = "Finca";
                precioF = this.precioI + this.precioI * 0.11f;
                this.mensualidades = 10 * 12;
                this.intereses = 0.11f;
                break;
            case "2":
                this.tipoLocal = "Solar";
                precioF = this.precioI + this.precioI * 0.11f;
                this.mensualidades = 10 * 12;
                this.intereses = 0.11f;
                break;
            case "3":
                this.tipoLocal = "Piso";
                if (this.comprador.toLowerCase().equals("funcionario")) {
                    precioF = this.precioI + this.precioI * 0.075f;
                    this.intereses = 0.075f;
                } else {
                    precioF = this.precioI + this.precioI * 0.105f;
                    this.intereses = 0.105f;
                }
                if (this.precioI <= 100000) {
                    this.mensualidades = 12 * 12;
                } else {
                    this.mensualidades = 20 * 12;
                }
                break;
            case "4":
                this.tipoLocal = "Casa";
                if (this.comprador.toLowerCase().equals("funcionario")) {
                    precioF = this.precioI + this.precioI * 0.075f;
                    this.intereses = 0.075f;
                } else {
                    precioF = this.precioI + this.precioI * 0.105f;
                    this.intereses = 0.105f;
                }
                if (this.precioI <= 100000) {
                    this.mensualidades = 12 * 12;
                } else {
                    this.mensualidades = 20 * 12;
                }
                break;
            case "5":
                this.tipoLocal = "Local comercial";
                precioF = this.precioI + this.precioI * 0.1f;
                this.mensualidades = 15 * 12;
                this.intereses = 0.1f;
                break;
            case "6":
                salir = true;
                break;
            default:
                error = "Error en tipo de vividenda.";
                break;

        }
        if (!salir) {
            Pantalla.visualizar(error, this, precioF);
        }
    }

    public String toString(float precioF) {
        return "Compra de : " + this.tipoLocal + "\n\nPrecio de venta = " + String.format("%.2f", this.precioI)
                + "€\nIntereses " + String.format("%.3f", this.intereses) + "% = " + String.format("%.2f", this.precioI * this.intereses)
                + "€\nPrecio total = " + String.format("%.2f", precioF) + "€\nA PAGAR " + this.mensualidades + " MENSUALIDADES\nDE "
                + String.format("%.2f", precioF / this.mensualidades) + "€\n";
    }
}

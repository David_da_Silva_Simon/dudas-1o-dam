package ejbiblioteca;

import java.util.Scanner;

/**
 *
 * @author a16daviddss
 */
public class EjBiblioteca {

    public static void main(String[] args) {

        String mes;
        int cp;
        String tit;
        byte nc;
        int tot;
        Scanner scan = new Scanner(System.in);
        System.out.printf("mes: ");
        mes=scan.next();
        System.out.printf("cantidad de libros prestados: ");
        cp=scan.nextInt();
        System.out.printf("título del libro: ");
        tit=scan.next();
        System.out.printf("número de consultas de libros en el mes: ");
        nc=scan.nextByte();
        System.out.printf("total de préstamos en un mes: ");
        tot=scan.nextInt(); 
        Biblioteca b = new Biblioteca(mes, cp, tit, nc, tot);
        System.out.printf("El libro %s se consultó %d veces."
                + "\nLa tasa de consulta en el mes de %s es %.2f%%\n",b.getTit(),b.getNc(),b.getMes(),b.tasaConsultas());
    }

}

/*
 *Ejercicio 1, programa que calcula el consumo de un camión cada 100km
 *crearemos dos métodos uno para la entrada llamado Entrada y otro para 
 *visualización llamado Visualización.
 */
package ejercicio1;

/**
 *
 * @author a16daviddss
 */
public class Ejercicio1 {

    public static void main(String[] args) {
        Entrada();

    }

    public static void Entrada() {
        String nombre = "Luis", matricula = "CD-092";
        float km1 = 3100f, km2 = 33300f, litrosGastados = 50f, PC;
        PC = (litrosGastados * 100) / (km_ult - km_penult);
        Visualizar(nombre, matricula, PC);

    }

    public static void Visualizar(String nombre, String matricula, float PC) {
        System.out.printf("Propietario: %s\n", nombre);
        System.out.printf("Matricula Camión: %s\n", matricula);
        System.out.printf("\tPorcentaje de consumo: %.2f%%\n", PC);

    }

}

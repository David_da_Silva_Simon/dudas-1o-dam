/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioinmobiliaria;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author a16daviddss
 */
public class Pantalla {

    public static void menu() throws IOException {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        String comprador = null;
        System.out.println("1-> Finca\n2-> Solar\n3-> Piso\n4-> Casa\n5-> Local comercial\n6-> Salir\n\nELIGE UNA OPCIÓN:");
        String tipo = read.readLine();
        if (!tipo.equalsIgnoreCase("salir")) {
            System.out.println("Introduce el precio inicial: ");
            float precio = Float.parseFloat(read.readLine());
            Local l = new Local(tipo, precio);
            if (tipo.equalsIgnoreCase("3") || tipo.equalsIgnoreCase("4")) {
                System.out.println("Introduce el tipo de comprador.");
                l.setComprador(comprador = read.readLine());
            }

            l.calculo();
        }
    }

    public static void visualizar(String err, Local l, float precioF) {
        if (err != null) {
            System.out.println(err);
        } else {
            System.out.println(l.toString(precioF));
        }
    }
}

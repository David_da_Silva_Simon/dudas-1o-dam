package bibliotecateclado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author davidarch
 */
public class BibliotecaTeclado {

    public static void main(String args[]) throws IOException {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        boolean correcto = true;
        String mes, tit;
        int cp, tot;
        byte nc;
        System.out.printf("Introduce el mes: ");
        mes = read.readLine();
        System.out.printf("Introduce la cantidad de libros prestados: ");
        cp = Integer.parseInt(read.readLine());
        System.out.printf("Introduce el titulo del libro: ");
        tit = read.readLine();
        System.out.printf("Introduce el numero de consultas de este mes: ");
        nc = Byte.parseByte(read.readLine());
        System.out.printf("Introduce el total de prestamos de este mes: ");
        tot = Integer.parseInt(read.readLine());
        Biblioteca b = new Biblioteca(mes, cp, tit, nc, tot);
        System.out.printf("El libro \"%s\" se consultó %d veces."
                + "\nLa tasa de consulta en el mes de %s es %.2f%%\n",
                b.getTit(), b.getNc(), b.getMes(), b.tasaConsultas());
    }

}

package ejerciciosuma1;

public class EjercicioSUma1 {

    public static void main(String[] args) {
        byte a = 30, b = 50;
        int suma;
        float c = 3.141516f, d = 42.85f; //el segundo
        //número no se si esta correcto 
        float suma2;
        byte suma3;

        suma = a + b;
        suma2 = c + d;
        suma3 = (byte) (c + d);
        System.out.printf("El resultado de suma uno es = %d\n", suma);
        System.out.printf("El resultado de suma2 es = %.2f\n", suma2); 
        //utilizamos .2 delante de f para que solo se muestren dos decimales
        System.out.printf("El resultado de suma 3 es = %d\n", suma3);

    }

}

/*
 *Metodo o funcion
 */
package funcion1;

/**
 *
 * @author a16daviddss
 */
public class Funcion1 {

    public static void main(String[] args) {
        int a = 50, b = 50, c = 10;
        //invocacion o llamada al metodo sumar
        //byte suma = (byte) sumar((byte) a, b);
        sumar(a, b);
        int suma = sumar1(a, b);
        System.out.println("La suma con sobrecarga es = " + suma);
        // System.out.println("La suma es = " + suma);

    }

    public static void sumar(int a, int b) {  //Prototipo o cabecera del metodo
        int suma = a + b;                   //cuerpo o implementacion del metodo
        visualizarSuma(suma);

    }

    public static int sumar1(int a, int b) {  //Prototipo o cabecera del metodo
        int suma = a + b;                   //cuerpo o implementacion del metodo
        //System.out.println("La suma con sobrecarga es = " + suma);
        //visualizarSuma(suma);
        return suma;
    }

    public static void visualizarSuma(int a) {
        System.out.println("La suma es = " + a);
    }

}

package bibliotecateclado;

/**
 *
 * @author davidarch
 */
public class Biblioteca {

    private String mes;
    private int cp;
    private String tit;
    private byte nc;
    private int tot;

    public Biblioteca() {
    }

    public Biblioteca(String mes, int cp, String tit, byte nc, int tot) {
        this.mes = mes;
        this.cp = cp;
        this.tit = tit;
        this.nc = nc;
        this.tot = tot;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getTit() {
        return tit;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public byte getNc() {
        return nc;
    }

    public void setNc(byte nc) {
        this.nc = nc;
    }

    public int getTot() {
        return tot;
    }

    public void setTot(int tot) {
        this.tot = tot;
    }

    public float tasaConsultas() {
        return (float) ((this.nc * 100) / this.tot);
    }
}
